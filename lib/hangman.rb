class Hangman
  attr_reader :guesser, :referee, :board

  def initialize(players)
    @guesser = players[:guesser]
    @referee = players[:referee]
  end

  def setup
    word_length = @referee.pick_secret_word
    @guesser.register_secret_length(word_length)
    @board = Array.new(word_length)
  end

  def take_turn
    try = @guesser.guess
    idx = @referee.check_guess(try)

    update_board(try, idx)
    @guesser.handle_response(try, idx)
  end

  def update_board(try, idx)
    idx.each {|i| @board[i] = try}
  end

end


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

class HumanPlayer

end

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


class ComputerPlayer
  attr_reader :candidate_words
  def initialize(dictionary = File.readlines('lib/dictionary.txt'))
    @dictionary = dictionary
  end

  def pick_secret_word
    @secret_word = @dictionary.sample
    @secret_word.length
  end

  def check_guess(letter)
    arr = @secret_word.split('')
    idx = []
    arr.each_index do |i|
      if arr[i] == letter
        idx << i
      end
    end
    idx
  end

  def guess(board)
    most_freq_let(board)

  end

  def most_freq_let(board)
    freq = Hash.new(0)
    @candidate_words.each do |word|
      board.each_with_index do |let, i|
        freq[word[i]] += 1 if let.nil?
      end
    end

    freq.max_by{|k,v| v}[0]

  end

  def handle_response(guess, idx)
    if idx.empty?
      @candidate_words = @candidate_words.select! {|w| w.include?(guess) == false}
    else
      @candidate_words = @candidate_words.select! {|w| usable_word?(w, guess, idx)}
    end
  end

  def usable_word?(word, letter, indices)
    word.chars.each_with_index do |char, idx|
      return false if char == letter && !indices.include?(idx)
      return false if indices.include?(idx) && char != letter
    end
    true
  end

  def register_secret_length(length)
     @candidate_words = @dictionary.select { |word| word.length == length }
  end

end
